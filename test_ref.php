<?php namespace VidIva; ?>

<!DOCTYPE html>
<!--
Work sample:    Inrego - Defect report form and charts
Version:        1.0
Author:         Ivan Videsvall
Date:           161025

Beställning:
  En avdelning har problem att hålla koll på fel på leveranser.
  Visa en lista på leveranserna där man ska kunna markera och lägga till fel på produkterna,
  på varje leverans finns det olika artikeltyper.
  Man behöver kunna registrera fel på leverans med artikeltyp och antal, detta kan göras flera gånger
  på samma leverans och sparas med en tidsstämpel. Felen behöver inte sparas permanent utan det räckar att dom
  finns i webbläsaren.
  Antalet fel på dom olika artikeltyperrna ska synas direkt på leveransraderna efter registreringen, utan att
  sidan laddas om eller via ajax.
  Under leveranserna ska det finnas en knapp med texten "Rapport", som ska samla ihop felen och skicka till
  samma sida i php som genererar ett cirkeldiagram i css med hjälp av php på antal fel per artikeltyp,
  samt ett barchart diagram med procentuellt antal leveranser som innehåller fel per månad.
  Ett tips för cirkel och barchart diagrammen är att använda svg istället för en ren css lösning.

Begränsning:
  Inga andra bibliotek än jquery får användas.
  All css, js och php ska finnas i denna fil som är den enda som får finnas eller skapas.
  Ändringar får ej göras på $deliveries och delivery_rows
-->
<html>
<head>
    <meta charset="utf-8">
    <title>Test 2016</title>
    <style>
        table, th, td {
            border: 1px solid #ccc;
            border-collapse: collapse;
        }

        th, td {
            padding: 6px 8px;
        }

        /* alternate row background colour */
        table.components tbody tr:nth-child(odd) {
            background-color: #eee;
        }

        /* suppress number input spinners */
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type=number] {
            -moz-appearance:textfield;
        }

        button {
            margin: 6px 0;
        }

        figure {
            border: 2px solid #aaa;
            padding: 10px 20px;
        }

        figure.pie {
            width: 400px;
        }

        figcaption {
            padding: 10px;
            font-weight: bold;
        }

        .legend {
            background-color: #eee;
            margin: 10px;
            padding: 10px 0;
        }

        .legend li {
            list-style-type: square;
            font-size: 1.5em;
        }

        .legend span {
            color: black;
            font-size: initial;
        }
    </style>
</head>

<?php

//Får ej ändras i
$deliveries = [
    ['id'=>1001,'date'=>'2016-01-01','name'=>'Ikea'],
    ['id'=>1002,'date'=>'2016-01-02','name'=>'Volvo'],
    ['id'=>1003,'date'=>'2016-01-05','name'=>'Sparbanken'],
    ['id'=>1004,'date'=>'2016-01-08','name'=>'Nissan'],
    ['id'=>1005,'date'=>'2016-02-01','name'=>'Cafe Opera'],
    ['id'=>1006,'date'=>'2016-02-02','name'=>'Hemköp'],
    ['id'=>1007,'date'=>'2016-02-01','name'=>'Hanssons Stug AB'],
    ['id'=>1009,'date'=>'2016-03-06','name'=>'Stenmark AB'],
    ['id'=>1010,'date'=>'2016-03-07','name'=>'Pizzeria Globen'],
    ['id'=>2001,'date'=>'2016-03-11','name'=>'Zendable AB'],
    ];
//Får ej ändras i
$delivery_rows =[
    ['id'=>30001,'delivery_id'=>1001,'article_type'=>'pc'     ,'qty'=>50],
    ['id'=>30002,'delivery_id'=>1001,'article_type'=>'bb'     ,'qty'=>40],
    ['id'=>30003,'delivery_id'=>1002,'article_type'=>'bb'     ,'qty'=>120],
    ['id'=>30004,'delivery_id'=>1003,'article_type'=>'mobile' ,'qty'=>130],
    ['id'=>30005,'delivery_id'=>1004,'article_type'=>'pc'     ,'qty'=>50],
    ['id'=>30006,'delivery_id'=>1004,'article_type'=>'bb'     ,'qty'=>2],
    ['id'=>30007,'delivery_id'=>1005,'article_type'=>'pc'     ,'qty'=>10],
    ['id'=>30008,'delivery_id'=>1006,'article_type'=>'mobile' ,'qty'=>10],
    ['id'=>30009,'delivery_id'=>1006,'article_type'=>'pc'     ,'qty'=>10],
    ['id'=>30010,'delivery_id'=>1007,'article_type'=>'pc'     ,'qty'=>12],
    ['id'=>30011,'delivery_id'=>1009,'article_type'=>'mobile' ,'qty'=>10],
    ['id'=>30012,'delivery_id'=>1009,'article_type'=>'pc'     ,'qty'=>44],
    ['id'=>30013,'delivery_id'=>1010,'article_type'=>'pc'     ,'qty'=>20],
    ['id'=>30014,'delivery_id'=>1010,'article_type'=>'bb'     ,'qty'=>50],
    ['id'=>30015,'delivery_id'=>1010,'article_type'=>'mobile' ,'qty'=>10],
    ['id'=>30016,'delivery_id'=>2001,'article_type'=>'pc'     ,'qty'=>40],
    ['id'=>30017,'delivery_id'=>2001,'article_type'=>'pc'     ,'qty'=>4],
    ];

/**
 * This class contains utility functions.
 */
class Misc
{
    /**
     * Generates a random hex colour string.
     * @return String  The colour string
     */
    public static function getRandHexColour() {

        $rand = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
        return '#' . $rand[rand(6, 15)] . $rand[rand(0, 15)] . $rand[rand(6, 15)] .
                $rand[rand(0, 15)] . $rand[rand(6, 15)] . $rand[rand(0, 15)];
    }
}

/**
 * This class handles the rendering of the Defect report form.
 */
class Defects
{
    /**
     * This function converts any HTML tags into plain text.
     * @param  string $data The input string.
     * @return string       The processed input string.
     */
    private static function safeText($data) {

        return htmlentities($data, ENT_COMPAT, 'UTF-8');
    }

    /**
     * The delivery components table markup is rendered by this function.
     * @param  int $del_id The delivery ID.
     */
    private static function renderComponentsTable($del_id)
    {
        global $delivery_rows;
        // delivery components filtered by delivery ID
        $delivery_components = array_filter($delivery_rows, function($row) use ($del_id) {

            return $row['delivery_id'] == $del_id['id'];
        }); ?>

        <table class="components">
            <thead>
                <tr>
                    <th>Component ID</th>
                    <th>Article</th>
                    <th>Amount</th>
                    <th>Total defects recorded</th>
                    <th>Number of defects to add</th>
                    <th>Revision time</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($delivery_components as $component) { ?>
                    <tr>
                        <td><?= self::safeText($component['id']) ?></td>
                        <td data-article><?= self::safeText($component['article_type']) ?></td>
                        <td><?= self::safeText($component['qty']) ?></td>
                        <td class="js-accumulator"></td>
                        <td>
                            <input type="number">
                        </td>
                        <td class="js-timestamp"></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php }

    /**
     * The delivery table markup is rendered by this function.
     */
    public static function renderTable()
    {
        global $deliveries;
        ?>
        <table>
            <thead>
                <tr>
                    <th>Delivery ID</th>
                    <th>Recipient</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($deliveries as $del_id) { ?>
                    <tr>
                        <td><?= self::safeText($del_id['id']) ?></td>
                        <td><?= self::safeText($del_id['name']) ?></td>
                        <td><?= self::safeText($del_id['date']) ?></td>
                    </tr>
                    <tr data-delivery-month="<?= substr(self::safeText($del_id['date']), 5, 2) * 1 ?>">
                        <td></td>
                        <td colspan="2">
                            <?php self::renderComponentsTable($del_id) ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php }
}

/**
 * This class renders charts of different kinds.
 */
class Charts {

    /**
     * This function renders a pie chart with the total number of defective
     * articles by article type.
     */
    private static function pieChart() {

        $articles = json_decode($_GET['articles'], true);
        $valueSum = array_sum($articles);
        $caption = 'Defective items by article type';
        $legend = []; ?>

        <figure class="pie">
            <figcaption><?= $caption ?></figcaption>

            <svg viewbox="0 0 64 64" style="transform: rotate(-.25turn)">
                <title><?= $caption ?></title>
                <?php
                $offset = 0;
                $arc = 0;
                foreach ($articles as $key => $value) {
                    $legend[$key] = [$value, $colour = Misc::getRandHexColour()]?>
                    <circle r="25%" cx="50%" cy="50%" stroke-dashoffset="<?= -($offset += $arc) ?>"
                            stroke-dasharray="<?= $arc = .5 + $value * 100 / $valueSum ?> 100"
                            stroke="<?= $colour ?>" fill="none" stroke-width="32"/>
                <?php }
                ?>
            </svg>
            <div class='legend'>
                <ul>
                    <?php foreach ($legend as $article => $value) { ?>
                        <li style="color: <?= $value[1] ?>">
                            <span><?= $article . ': ' . $value[0] ?></span>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </figure>
    <?php }

    /**
     * This function renders a bar chart based on the ratio of defective
     * deliveries by month. The data is extracted from the $_GET['months'] parameter.
     * It utilizes SVG for rendering graphics.
     */
    private static function barChart() {

        $months = json_decode($_GET['months'], true);

        $figWidth = 800;
        $maxBarWidth = $figWidth * .8;
        $barHeight = $figWidth / 2 / sizeof($months) - 1;
        $caption = 'Deliveries containing defects by month';
        date_default_timezone_set('Europe/Stockholm');
        ?>

        <figure>
            <figcaption><?= $caption ?></figcaption>
            <svg width="<?= $figWidth ?>" height="<?= $figWidth / 2 ?>">
                <title><?= $caption ?></title>
                <?php
                $count = 0;
                foreach ($months as $key => $value) {

                    $dateObj = \DateTime::createFromFormat('!m', $key);
                    $monthName = $dateObj->format('F');
                    ?>
                    <g transform="translate(0, <?= $barHeight * $count++ ?>)">
                        <rect width="<?= $maxBarWidth * $value['defective'] / $value['delivered'] ?>"
                              height="<?= $barHeight ?>" fill="<?= Misc::getRandHexColour() ?>"/>
                        <text x="<?= $maxBarWidth * $value['defective'] / $value['delivered'] + 5 ?>"
                              y="<?= $barHeight / 2 ?>" dy=".35em">
                              <?= $monthName . ' ' . round($value['defective'] * 100 / $value['delivered'], 2) . ' %' ?>
                        </text>
                    </g>
                <?php } ?>
            </svg>
        </figure>
    <?php }

    /**
     * This function renders all available charts.
     */
    public static function renderCharts() {

        self::pieChart();
        self::barChart();
    }
} ?>

<!-- HTML markup starts here. -->
<body>
    <?php
    if (!isset($_GET['articles'])) {
        echo '<h2>Defect report form</h2>';
        Defects::renderTable();
        echo '<button>Report</button>';
    } else {
        Charts::renderCharts();
    } ?>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script type="text/javascript">
        /**
         * Project: Defects
         * Module: VidIva
         */
        var Defects = Defects || {};
        Defects.VidIva = (function defects() {

            /**
             * Binds an envent handler to all input elements. Upon change the event
             * handler function _updateRow() is called.
             */
            function init() {

                $('input').change(_updateRow);
                $('button').click(_submitData);
            }

            /**
             * This function updates the accumulator field, set a new timestamp and
             * resets input field for the appropriate article row. It also records
             * each number added and the associated timestamp in an array stored in
             * the <input> DOM element currently used.
             */
            function _updateRow() {

                var $input = $(this);
                var $accumulator = $input.parent().siblings('.js-accumulator');
                var $timestamp = $input.parent().siblings('.js-timestamp');
                var inputNum = parseInt($input.val(), 10);
                var accumulatorNum = parseInt($accumulator.text(), 10) || 0;

                $accumulator.text(accumulatorNum + inputNum);
                $input.val('');
                timestamp = new Date().toLocaleString();
                $timestamp.text(timestamp);

                // append input record with timestamp and number
                // the array can be retrieved using $(this).data('updates')
                if (typeof $(this).data("updates") === 'undefined') {
                    $(this).data('updates', [[timestamp, inputNum]]);
                } else {
                    $(this).data('updates').push([timestamp, inputNum]);
                };
            }

            /**
             * This function returns an object containing the sum of defective
             * items by type of article.
             * @return Object   Defect sum per article type.
             */
            function _getArticles() {

                // summary of defects per articles are stored here
                var articleSummary = {};
                // table rows with modified articles
                var $articles = $('.components tbody tr').filter(function () { return $('.js-accumulator', this).text() !== ''});
                // accumulate totals for each article type
                $articles.each(function () {

                    var key = $('td[data-article]', this).text();
                    var val = parseInt($('.js-accumulator', this).text(), 10);

                    articleSummary[key] = (articleSummary[key] || 0) + val;
                });

                return articleSummary;
            }

            /**
             * This function returns an object containing the share of defective
             * deliveries per month.
             * @return Object   Defect deliveries share by month, on the form {"mm": {"defective":n,"delivered":m}}.
             */
            function _getMonths () {

                // record of defective deliveries by month
                var month = {};
                // the deliveries
                var $deliveries = $('tr[data-delivery-month]');
                // record deliveries and any deliveries having defective articles for each month
                $deliveries.each(function () {

                    // the month as an integer string, e.g. "12"
                    var key = $(this).data('delivery-month');
                    // prime the object property if it doesn't exist
                    if (typeof month[key] === 'undefined') month[key] = {'defective': 0, 'delivered': 0};
                    var defectCount = month[key]['defective'];
                    // detect if any article defect accumulator is non-empty
                    if ($('.js-accumulator', this).text() !== "") defectCount++;
                    month[key] = {'defective': defectCount, 'delivered': month[key]['delivered'] + 1};
                });

                return month;
            }

            /**
             * This function reloads the page with JSON representation of chart data as GET parameters.
             */
            function _submitData() {

                window.location.href = window.location.origin + window.location.pathname + '?articles=' + JSON.stringify(_getArticles()) + '&months=' + JSON.stringify(_getMonths());
            }

            /**
             * The module API.
             *
             * This is used to activate the module.
             */
            return {
                init: init
            }
        })();

        // activate the module after document load
        $(document).ready(function documentReady() {

            Defects.VidIva.init();
    });
    </script>
</body>
</html>
